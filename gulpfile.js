// -------------------------------------
//
//   Gulpfile
//
// -------------------------------------
//
// Available tasks:
//   `gulp`
//   `gulp browserSync`
//   `gulp build`
//   `gulp compile:js`
//   `gulp compile:sass`
//   `gulp compile:svgSprite`
//   `gulp lint:js`
//   `gulp lint:sass`
//   `gulp minify:css`
//   `gulp minify:js`
//   `gulp minify:svgs`
//   `gulp serve`
//   `gulp watch`
//
// -------------------------------------


// -------------------------------------
//   Modules - devDependencies
// -------------------------------------
//
// babel-core                 : Babel compiler core.
// babel-preset-env           : A Babel preset for each environment.
// browser-sync               : Time-saving browser testing.
// eslint                     : JavaScript code quality tool.
// eslint-config-airbnb       : Airbnb's eslintrc shared config.
// eslint-config-prettier     : Turns off all rules that might conflict with Prettier.
// eslint-plugin-import       : Support linting of ES2015+ (ES6+) import/export syntax.
// eslint-plugin-jsx-a11y     : Static AST checker for accessibility rules on JSX elements.
// eslint-plugin-prettier     : Runs Prettier as an ESLint rule and reports differences as individual ESLint issues.
// eslint-plugin-react:       : React specific linting rules for ESLint.
// gulp                       : The streaming build system.
// gulp-autoprefixer          : Prefix CSS.
// gulp-babel                 : Use of next generation JavaScript.
// gulp-clean-css             : Minify CSS.
// gulp-concat                : Concatenate files.
// gulp-eslint                : A gulp plugin for ESLint.
// gulp-if                    : Conditionally control the flow of vinyl objects.
// gulp-load-plugins          : Automatically load Gulp plugins.
// gulp-notify                : Notification plugin for gulp.
// gulp-plumber               : Prevent pipe breaking from errors.
// gulp-postcss               : Gulp plugin to pipe CSS through several plugins, but parse CSS only once.
// gulp-rename                : Rename files.
// gulp-sass                  : Compile Sass.
// gulp-sourcemaps            : Generate sourcemaps.
// gulp-stylelint             : A plugin that runs stylelint results through a list of reporters.
// gulp-svg-sprite            : Takes SVG files, optimizes them and bakes them into SVG sprites.
// gulp-svg-min               : Minifies SVG files.
// gulp-uglify                : Minify JavaScript with UglifyJS.
// postcss-reporter           : PostCSS plugin to console.log() the messages registered by other PostCSS plugins.
// postcss-scss               : A Sass parser for PostCSS.
// prettier                   : An opinionated code formatter.
// run-sequence               : Run a series of dependent Gulp tasks in order.
// stylelint                  : Linter that helps you avoid errors and enforce conventions in your styles.
// stylelint-config-standard  : The standard shareable config for stylelint
// stylelint-order            : A plugin pack of order related linting rules for stylelint
//
// -------------------------------------

// -------------------------------------
//   Modules - dependencies
// -------------------------------------
//
// foundation-sites           :
//
// -------------------------------------

/* global require */
var gulp = require('gulp');

// Setting pattern this way allows non gulp- plugins to be loaded as well.
var plugins = require('gulp-load-plugins')({
  pattern: '*',
  rename: {
    'browser-sync':     'browserSync',
    'gulp-add-src':     'addSrc',
    'gulp-babel':       'babel',
    'gulp-clean-css':   'cleanCSS',
    'gulp-eslint':      'gulpEslint',
    'gulp-stylelint':   'gulpStylelint',
    'gulp-postcss' :    'postcss',
    'postcss-reporter': 'reporter',
    'run-sequence':     'runSequence',
    'postcss-scss':     'scss'
  }
});

// !/usr/bin/env node
var argv = require('yargs').argv;

// Used to check development context.
var environment = (argv.prod === undefined) ? 'dev' : 'prod';

// Used to generate relative paths.
var path = require('path');

// Paths used in options below.
var paths = {
  sass: {
    source: 'src/sass/',
    destination: 'dist/css/'
  },
  css: {
    source: 'dist/css/',
    destination: 'dist/css/'
  },
  js: {
    source: 'src/js/',
    destination: 'dist/js/'
  },
  images: {
    source: 'src/images/',
    destination: 'dist/images/',
    svgs: {
      source: 'src/images/svg/',
      destination: 'dist/images/svg/'
    }
  },
  fonts: {
    source: 'src/fonts/',
    destination: 'dist/fonts/'
  },
  templates: {
    source: 'templates/'
  }
};

// Options passed to each task.
var options = {

  // ----- Environment ----- //

  environment: environment,

  // ----- Browsersync ----- //

   browserSync: {
     files: [
       path.join(paths.sass.destination, '**/*.css'), 
       path.join(paths.js.destination, '**/*.js'),
       path.join(paths.templates.source, '**/*.twig')
     ],
     proxy: {
       target: 'http://km-playground.test/'
     },
     logConnections: true
   },

  // ----- Sass ----- //

  sass: {
    files: path.join(paths.sass.source, '**/*.scss'),
    destination: path.join(paths.sass.destination)
  },

  // ----- CSS ----- //

  css: {
    files: [
      paths.css.source + '**/*.css',
      '!' + paths.css.source + '**/*.min.css'
    ],
    destination: path.join(paths.css.destination)
  },

  // ----- JS ----- //

  js: {
    files: {
      source: path.join(paths.js.source, '**/*.es6.js')
    },
    file: path.join(paths.js.destination, 'main.js'),
    destination: path.join(paths.js.destination)
  },

  // ----- Images ----- //

  images: {
    files: path.join(paths.images.source, '**/*'),
    svgs: {
      files: path.join(paths.images.svgs.source + '**/*.svg'),
      destination: path.join(paths.images.svgs.destination)
    },
    destination: path.join(paths.images.destination)
  },

  // ----- Fonts ----- //

  fonts: {
    files: path.join(paths.fonts.source, '**/*'),
    destination: path.join(paths.fonts.destination)
  }
};

console.log(options.environment);

// Function: Report errors in tasks.
const reportError = function (error) {
  let errorMessage = '';

  if (error.plugin) {
    errorMessage += error.plugin;
  }
  if (error.message) {
    errorMessage += error.message;
  }

  console.error(errorMessage.toString());

  plugins.notify({
    title: 'Error in plugin: ' + error.plugin,
    message: errorMessage,
    sound: 'Basso'
  }).write(error);

  // Prevent the 'watch' task from stopping.
  this.emit('end');
};

// Tasks.
require('./gulp-tasks/browser-sync')(gulp, plugins, options);
require('./gulp-tasks/build')(gulp, plugins, options);
require('./gulp-tasks/clean')(gulp, plugins, options, reportError);
require('./gulp-tasks/compile-sass')(gulp, plugins, options, reportError);
require('./gulp-tasks/compile-js')(gulp, plugins, options);
require('./gulp-tasks/compile-svg-sprite')(gulp, plugins, options);
require('./gulp-tasks/default')(gulp, plugins, options);
require('./gulp-tasks/copy-assets')(gulp, plugins, options);
require('./gulp-tasks/lint-sass')(gulp, plugins, options, reportError);
require('./gulp-tasks/lint-js')(gulp, plugins, options, reportError);
require('./gulp-tasks/minify-css')(gulp, plugins, options);
require('./gulp-tasks/minify-js')(gulp, plugins, options);
require('./gulp-tasks/minify-svgs')(gulp, plugins, options);
require('./gulp-tasks/serve')(gulp, plugins, options);
require('./gulp-tasks/watch')(gulp, plugins, options);
