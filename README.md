# kodamera-theme-playground

A Drupal 8 base theme intended to be used as a playground and creative space.

[Drupal 8 theming](https://www.drupal.org/docs/8/theming)

## Frontend build tools - our own custom setup

We use [Yarn](https://yarnpkg.com/en/) for dependency management and [Gulp](https://gulpjs.com/) as streaming build system for our frontend build tasks.

All available Gulp-tasks are located in the `gulp-tasks` folder.

## CSS & Sass

We write SCSS syntax and we do it in BEM-style, some handy BEM-resources:

[Get your head around the BEM syntax](https://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)

[Taking the BEM naming convention a step further](https://csswizardry.com/2015/08/bemit-taking-the-bem-naming-convention-a-step-further/)


### CSS methodology

We follow [Sass guidelines](https://sass-guidelin.es) by Hugo Giraudel for writing sane, maintainable and scalable Sass.

### CSS Framework

We include the [Foundation framework](https://foundation.zurb.com/sites/docs/xy-grid.html) (XY-grid) to quickly get going with a simple grid.

### Stylelint

We extend the [Stylelint config standard](https://github.com/stylelint/stylelint-config-standard) when we lint our SCSS along with some of our own rules specified in `stylelintrc.yml`

## JavaScript

We write `es6` which we compile down to `es5` with Babel.

[Drupal 8 JavaScript API](https://www.drupal.org/docs/8/api/javascript-api/javascript-api-overview)

### ESlint

We extend Drupal Core's JavaScript linter which in turn extends [Airbnb's JavaScript linter](https://github.com/airbnb/javascript).

Core also extends [Prettier](https://github.com/prettier/prettier-eslint) that lints our JavaScript format.

