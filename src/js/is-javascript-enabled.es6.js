(($, { behaviors }) => {
  /**
   * Removes no-js class if JavaScript is enabled.
   * @type {Object}
   */
  behaviors.isJavascriptEnabled = {
    attach(context) {
      $(".js-body", context).removeClass("no-js");
    }
  };
})(jQuery, Drupal);
