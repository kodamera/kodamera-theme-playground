(($, { behaviors }) => {
  /**
   * Toggles main menu
   */
  behaviors.navigation = {
    attach(context) {
      const $subMenuParent = $(".js-menu-item--has-sub-menu", context);
      const $subMenu = $(".js-menu-submenu", context);
      const $navigationToggleOpen = $(".js-navigation__toggle--open", context);
      const $navigationToggleClose = $(
        ".js-navigation__toggle--close",
        context
      );
      const $navigationWrapper = $(".js-navigation__wrapper", context);

      $navigationToggleOpen
        .once("navigation-toggle-open-button-pressed")
        .click(event => {
          const $this = $(event.currentTarget);

          $navigationToggleClose.focus();
          $this.attr("aria-expanded", "true");
          $navigationWrapper.addClass("is-active");
          setTimeout(() => {
            $navigationToggleClose.focus();
          }, 375);
        });

      $navigationToggleClose
        .once("navigation-toggle-close-button-pressed")
        .click(event => {
          const $this = $(event.currentTarget);
          $navigationWrapper.removeClass("is-active");
          $navigationToggleOpen.attr("aria-expanded", "false").focus();
          $this.blur();
        });

      $subMenuParent.once("sub-menu-parent-pressed").click(event => {
        const $this = $(event.currentTarget);

        $this.toggleClass("is-expanded");
        $this.find($subMenu).slideToggle();
      });

      $subMenuParent.keypress(event => {
        const $this = $(event.currentTarget);

        if (event.which === 13) {
          $this.toggleClass("is-expanded");
          $this.find($subMenu).slideToggle();
        }
      });
    }
  };
})(jQuery, Drupal);
