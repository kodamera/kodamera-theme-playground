/**
 * @file
 * Task: Copy: Assets.
 */

/* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('copy:assets', ['copy:images', 'copy:fonts']);

  gulp.task('copy:images', function () {
    return gulp.src(options.images.files)
      .pipe(gulp.dest(options.images.destination));
  });

  gulp.task('copy:fonts', function () {
    return gulp.src(options.fonts.files)
      .pipe(gulp.dest(options.fonts.destination));
  });
};
