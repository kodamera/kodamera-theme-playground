/**
 * @file
 * Task: Lint: Sass.
 */

/* global module */

module.exports = function (gulp, plugins, options, reportError) {
  'use strict';

  gulp.task('lint:sass', function () {
    var preLintSass = [
      plugins.stylelint({
       failAfterError: true,
       reporters: [
         {
           formatter: 'verbose',
           console: true
         }
       ]
     }),
      plugins.reporter({
        clearReportedMessages: true,
        throwError: true
      })
    ];

    return gulp.src(options.sass.files)
      .pipe(plugins.plumber({ errorHandler: reportError }))
      .pipe(plugins.postcss(preLintSass, {syntax: plugins.scss}))
     .pipe(plugins.plumber.stop());
  });

  gulp.task('lint:sass-with-fail', function () {
   return gulp.src(options.sass.files)
     .pipe(plugins.gulpStylelint({
       failAfterError: true,
       reporters: [
         {
           formatter: 'verbose',
           console: true
         }
       ]
     }))
  });
};
