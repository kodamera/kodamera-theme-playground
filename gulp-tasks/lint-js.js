/**
 * @file
 * Task: Lint: ES6.
 */

/* global module */

module.exports = function (gulp, plugins, options, reportError) {
  'use strict';

  gulp.task('lint:js', function () {
    return gulp.src(options.js.files.source)
      .pipe(plugins.plumber({ errorHandler: reportError }))
      .pipe(plugins.gulpEslint())
      .pipe(plugins.gulpEslint.format())
      .pipe(plugins.gulpEslint.failAfterError())
      .pipe(plugins.plumber.stop());
  });

  gulp.task('lint:js-with-fail', function () {
    return gulp.src(options.js.files.source)
      .pipe(plugins.gulpEslint())
      .pipe(plugins.gulpEslint.format())
      .pipe(plugins.gulpEslint.failOnError());
  });
};
