/**
 * @file
 * Task: Minify: SVG files.
 */

/* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('minify:svgs', function () {
    return gulp.src(options.images.svgs.files, { base: './' })
      .pipe(plugins.plumber({}))
      .pipe(plugins.svgmin())
  });
};