/**
 * @file
 * Task: Build.
 */

 /* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('build', function(callback) {

    if (options.environment === 'prod') {
      plugins.runSequence(
        ['lint:sass-with-fail', 'lint:js-with-fail', 'copy:assets'],
        'compile:sass', 
        'compile:js',
        'compile:svg-sprite',
        'minify:css',
        'minify:js',
        callback);
    }
    else {
      plugins.runSequence(
        ['lint:sass', 'lint:js', 'copy:assets'],
        'compile:sass',
        'compile:js',
        'compile:svg-sprite',
        'minify:css',
        'minify:js',
        callback);
    }
  });
};
