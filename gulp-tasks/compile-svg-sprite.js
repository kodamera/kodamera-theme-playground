/**
 * @file
 * Task: Concat: SVG sprite.
 */

/* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('compile:svg-sprite', function () {

    var svgSpriteOptions = {
      shape: {
          dimension: {
            maxWidth: 64,
            maxHeight: 64
          },
          spacing: {
            padding: 0
          }
        },
      mode: {
        symbol: {
          dest: 'sprites',
          prefix: '.svg--%s',
          sprite: 'sprite.svg',
          example: true
        }
      }
    };

    return gulp.src(options.images.svgs.files)
      .pipe(plugins.plumber({}))
      .pipe(plugins.svgSprite(svgSpriteOptions))
      .pipe(gulp.dest(options.images.svgs.destination));
  });
};