/**
 * @file
 * Task: Minify: JS.
 */

/* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('minify:js', function () {
    return gulp.src(options.js.file)
      .pipe(plugins.plumber({}))
      .pipe(plugins.uglify())
      .pipe(gulp.dest(options.js.destination));
  });
};
