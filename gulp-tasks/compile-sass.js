/**
 * @file
 * Task: Compile: Sass.
 */

/* global module */

module.exports = function (gulp, plugins, options, reportError) {
  'use strict';

  var errorHandler = true;

  if (options.environment === 'dev') {
    errorHandler = reportError;
  }

  gulp.task('compile:sass', function () {
    return gulp.src([options.sass.files])
      .pipe(plugins.plumber({ errorHandler: errorHandler }))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.sass({
        includePaths: ['node_modules/foundation-sites/scss/']
      }))
      .pipe(plugins.autoprefixer({
        cascade: false
      }))
      .pipe(plugins.sourcemaps.write('.'))
      .pipe(gulp.dest(options.sass.destination));
  });
};
