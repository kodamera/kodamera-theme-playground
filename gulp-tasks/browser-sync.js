/**
 * @file
 * Task: Browsersync.
 */

/* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  const browserSync = plugins.browserSync.create();

  gulp.task('browserSync', function () {
    browserSync.init(options.browserSync);
  });

  gulp.task('browserSync:reload', function () {
    browserSync.reload();
  });
};