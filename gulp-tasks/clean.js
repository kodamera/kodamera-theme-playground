/**
 * @file
 * Task: Clean.
 */

/* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  // Clean CSS files.
  gulp.task('clean:css', function () {
    // You can use multiple globbing patterns as you would with `gulp.src`.
    plugins.del.sync([
      options.css.destination
    ]);
  });

  // Clean JS files.
  gulp.task('clean:js', function () {
    // You can use multiple globbing patterns as you would with `gulp.src`.
    plugins.del.sync([
      options.js.destination
    ]);
  });

  // Clean image files.
  gulp.task('clean:images', function () {
    // You can use multiple globbing patterns as you would with `gulp.src`.
    plugins.del.sync([
      options.images.destination + 'svg/*.svg',
    ]);
  });
};
