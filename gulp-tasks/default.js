/**
 * @file
 * Task: Default.
 */

 /* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('default', ['build']);
};