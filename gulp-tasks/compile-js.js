/**
 * @file
 * Task: Compile: JavaScript.
 */

/* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('compile:js', function () {
    return gulp.src(options.js.files.source)
      .pipe(plugins.plumber({}))
      .pipe(plugins.babel({
        presets: [['env', {
          modules: false,
          useBuiltIns: true,
          targets: { browsers: ["last 2 versions", "> 1%"] },
        }]],
      }))
      .pipe(plugins.rename(file => (bundleName(file))))
      .pipe(plugins.concat('main.js'))
      .pipe(gulp.dest(options.js.destination));
  });

  /**
   * Translate *.es6.js files to *.js
   */
  const bundleName = (file) => {
    file.basename = file.basename.replace('.es6', '');
    file.extname = '.js';
    return file;
  };
};
