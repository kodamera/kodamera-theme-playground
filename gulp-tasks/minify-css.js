/**
 * @file
 * Task: Minify: CSS.
 */

/* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('minify:css', function () {
    return gulp.src(options.css.files)
      .pipe(plugins.plumber({}))
      .pipe(plugins.cleanCSS())
      .pipe(gulp.dest(options.css.destination));
  });
};
