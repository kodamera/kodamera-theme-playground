/**
 * @file
 * Task: Watch.
 */

/* global module */

module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('watch', ['watch:sass', 'watch:js']);

  gulp.task('watch:sass', function () {
    return gulp.watch([
      options.sass.files
    ], function () {
      plugins.runSequence(
        ['lint:sass', 'compile:sass']      
      );
    });
  });

  gulp.task('watch:js', function () {
    return gulp.watch([
      options.js.files.source
    ], function () {
      plugins.runSequence(
        'lint:js',
        'compile:js'
      );
    });
  });
};
